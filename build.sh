#!/bin/bash
for platform in ios android; do
  echo "Building $platform"
  cordova platforms add $platform
  cordova build $platform
  plugman install --platform $platform --project platforms/$platform --plugin https://github.com/npow/ScreenDim.git
  cordova build $platform
done
