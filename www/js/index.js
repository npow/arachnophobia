var storageKey = 'arachnaphobia_curLevel';

var app = {
    curLevel: parseInt(localStorage.getItem(storageKey), 10) || 0,

    images: [
      ['1-1B.jpg'],
      ['2-1.jpg', '2-2.jpg', '2-3.jpg'],
      ['3-1.jpg', '3-2.jpg'],
      ['4-1.jpg', '4-2.jpg'],
      ['5-1.jpg', '5-2.jpg', '5-3.jpg']
    ],

    initialize: function() {
        this.bindEvents();
        var windowHeight = parseInt($(window).height(), 10);
        var windowWidth = parseInt($(window).width(), 10);

        var fontsize = 20;
        var preferredHeight = 480;  
        var percentage = windowHeight / preferredHeight;
        var newFontSize = Math.floor(fontsize * percentage) - 1;
        $('body').css("font-size", newFontSize);        
        $(':button').css("font-size", newFontSize);        

        var rowHeight = '3em';
        $('body').height(windowHeight - 20);
        $('#slider').css('height', rowHeight);
        $(':button').css('height', rowHeight);
        $(':button').css('width', '80%');
        $('#main_img').attr('height', windowHeight - 1.5 * $('#slider').height());
    },

    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
        $('#continue_btn').click(function () {
          var val = $('#slider').slider('value');
          // advance to next level only if comfortable
          if (val < 50) {
            app.curLevel += 1;
          }
          if (app.curLevel < app.images.length) {
            app.displayImage();
          } else {
            // done with treatment
            app.displayEndScreen();
          }
          localStorage.setItem(storageKey, app.curLevel);
        });
        $('#start_btn').click(function () {
          $('#instructionsDivContainer').hide();
          $('#contentDiv').show();
          app.displayImage();
        });
        $('#restart_btn').click(function () {
          app.displayInstructions();
        });
    },

    onDeviceReady: function() {
        app.displayInstructions();
    },

    displayImage: function () {
        $('#anxietyDivContainer').hide();
        $('#contentDiv').show();
        $('#main_img').css('visibility', 'hidden');
        var imgs = app.images[app.curLevel];
        var idx = Math.floor(Math.random() * imgs.length);
        var fileName = imgs[idx];

        if (fileName.split('.').pop() === 'm4v') {
          $('#main_img').hide();
          $('#main_video').attr('src', 'img/' + fileName);
          $('#main_video').show();
        } else {
          $('#main_video').hide();
          $('#main_img').attr('src', 'img/' + fileName);
          $('#main_img').css('visibility', 'visible');
          $('#main_img').show();
        }
        app.displayProgressBar();
    },

    displayProgressBar: function () {
      var progressbar;
      function headerBar() {
          progressbar = $('#timerBar');
          progressbar.progressbar({
              value: 0
          });
          progressbar.css({ 'background': 'LightGreen' });
          $('#timerBar > div').css({ 'background': 'Green' })

          function progress() {
              var val = progressbar.progressbar('value') || 0;
              progressbar.progressbar('value', val + (10/3.0));
              if (val < 100) {
                  setTimeout(progress, 1000);
              } else {
                app.displaySlider();
              }
          }
          progress();
      }
      headerBar();
    },

    displaySlider: function () {
      $('#continue_btn').hide();
      $('#contentDiv').hide();
      $('#anxietyDivContainer').show();
      $('#slider').slider({
        change: function(event, ui) {
          $('a.ui-slider-handle').show();
          $('#continue_btn').show();
        }
      });
      $('a.ui-slider-handle').hide();
      $('#anxietyDivContainer').show();
    },

    displayInstructions: function () {
      $('#finalDivContainer').hide();
      $('#instructionsDivContainer').show();
    },

    displayEndScreen: function () {
      $('#continue_btn').hide();
      $('#contentDiv').hide();
      $('#anxietyDivContainer').hide();
      $('#finalDivContainer').show();

      app.curLevel = 0;
      localStorage.setItem(storageKey, app.curLevel);
    }
};

if (cordova.screenDim) {
    // turn off the default screen dimming
    document.getElementById('turnoff').addEventListener('touchstart', function (e) {
        cordova.screenDim.disable();
    });

    // turn it back on
    document.getElementById('turnon').addEventListener('touchstart', function (e) {
        cordova.screenDim.enable();
    });

    // toggle it
    document.getElementById('toggle').addEventListener('touchstart', function (e) {
        cordova.screenDim.toggle();
    });
}
